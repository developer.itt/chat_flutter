// ignore_for_file: unused_local_variable

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/constants/app_constants.dart';
import 'package:firebase_database/datamodel.dart';
import 'package:firebase_database/test.dart';
import 'package:firebase_database/textfield.dart';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';
// ignore: unused_import
import 'button.dart';
import 'test.dart';

class ListingScreenPage extends StatefulWidget {
  const ListingScreenPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<ListingScreenPage> createState() => _ListingScreenPageState();
}

class _ListingScreenPageState extends State<ListingScreenPage> {
  List<DataModel> array_userslist = [];
  int index = 0;
  final Stream<QuerySnapshot> users =
      FirebaseFirestore.instance.collection('users').snapshots();
  final prefs = SharedPreferences.getInstance();

  pushChatScreen(int index) {
    print('INDEX: $index');
    print('DONE');
    String peerId = '';
    String peerAvatar = '';
    String peerNickname = '';

//  Navigator.push(
//       context,
//       MaterialPageRoute(
//           builder: (context) => const TestScreenPage(
//                 title: 'Chatview',
//               )),
//     );
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => ChatPage(
    //       peerId: '1',
    //       peerAvatar: 'Saurav',
    //       peerNickname: '',
    //     ),
    //   ),
    // );

    //   Future<String> getId() async {
    //   const name = "Name";
    //   var user = prefs.getString(name);
    //   var username = '${user[0].toUpperCase()}${user.substring(1)}';
    //   print('get $username');
    //   return username;
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: StreamBuilder<QuerySnapshot>(
                stream: users,
                builder: (
                  BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot,
                ) {
                  if (snapshot.hasError) {
                    return const Text('');
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Text('');
                  }
                  final data = snapshot.requireData;
                  print(data.size);

                  array_userslist = [];
                  for (var i = 0; i < data.size; i++) {
                    if (data.docs[i]['Id'] != LogineUserId) {
                      DataModel modal = DataModel(
                          name: data.docs[i]['Name'],
                          phone: data.docs[i]['Phone'],
                          email: data.docs[i]['Email'],
                          age: data.docs[i]['Age'],
                          city: data.docs[i]['City'],
                          id: data.docs[i]['Id']);
                      array_userslist.add(modal);
                    }
                  }
                  return Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: ListView.builder(
                            itemCount: array_userslist.toList().length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                onTap: () => {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => TestScreenPage(
                                              title: array_userslist[index].name,
                                              id: array_userslist[index].id)))
                                },
                                title: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 50,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          borderRadius: BorderRadius.circular(8)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            child:
                                                Text(array_userslist[index].name),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            })),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
