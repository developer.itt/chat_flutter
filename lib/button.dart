import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  String text;
  final Function clickevent;

  Button(this.text, this.clickevent);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      child: RaisedButton(
        color: Colors.blue,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          text,
          style: const TextStyle(fontSize: 20, color: Colors.white),
        ),
        onPressed: clickevent(),
      ),
    );
  }
}

class SmallButton extends StatelessWidget {
  String text;
  final Function clickevent;

  SmallButton(this.text, this.clickevent);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topRight,
      margin: const EdgeInsets.only(top: 0, right: 20, left: 20),
      child: FlatButton(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        padding: const EdgeInsets.only(left: 5, right: 5, top: 0, bottom: 0),
        color: Colors.transparent,
        child: Text(
          text,
          style: const TextStyle(
              fontSize: 15,
              color: Colors.black,
              backgroundColor: Colors.transparent),
          textAlign: TextAlign.right,
        ),
        onPressed: clickevent(),
      ),
    );
  }
}
