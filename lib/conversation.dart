// ignore_for_file: unused_local_variable

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/allusers.dart';
import 'package:firebase_database/constants/app_constants.dart';
import 'package:firebase_database/datamodel.dart';
import 'package:firebase_database/test.dart';
import 'package:firebase_database/textfield.dart';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';
// ignore: unused_import
import 'button.dart';
import 'test.dart';

class ConversationScreenPage extends StatefulWidget {
  const ConversationScreenPage({Key? key, required this.title})
      : super(key: key);
  final String title;

  @override
  State<ConversationScreenPage> createState() => _ConversationScreenPageState();
}

class _ConversationScreenPageState extends State<ConversationScreenPage> {
  List<ConversationModal> array_conversationlist = [];
  int index = 0;
  final Stream<QuerySnapshot> users =
      FirebaseFirestore.instance.collection('conversation').orderBy('timestamp', descending: true).snapshots();
  final prefs = SharedPreferences.getInstance();

  pushChatScreen(int index) {
    print('INDEX: $index');
    print('DONE');
    String peerId = '';
    String peerAvatar = '';
    String peerNickname = '';

//  Navigator.push(
//       context,
//       MaterialPageRoute(
//           builder: (context) => const TestScreenPage(
//                 title: 'Chatview',
//               )),
//     );
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //     builder: (context) => ChatPage(
    //       peerId: '1',
    //       peerAvatar: 'Saurav',
    //       peerNickname: '',
    //     ),
    //   ),
    // );

    //   Future<String> getId() async {
    //   const name = "Name";
    //   var user = prefs.getString(name);
    //   var username = '${user[0].toUpperCase()}${user.substring(1)}';
    //   print('get $username');
    //   return username;
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ListingScreenPage(
                          title: 'All users',
                        )),
              );
            },
          ),
          // add more IconButton
        ],

        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back),
        //   onPressed: () {
        //     /** Do something */
        //   },
        // ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: StreamBuilder<QuerySnapshot>(
                stream: users,
                builder: (
                  BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot,
                ) {
                  if (snapshot.hasError) {
                    return const Text('');
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Text('');
                  }
                  final data = snapshot.requireData;
                  print(data.size);

                  array_conversationlist = [];
                  for (var i = 0; i < data.size; i++) {
                    print(data.docs[i]['ownerID']);
                    print(data.docs[i]['sendUserId']);

                    if (data.docs[i]['ownerID'] == LogineUserId ||
                        data.docs[i]['sendUserId'] == LogineUserId) {
                      ConversationModal modal = ConversationModal(
                          datetime: data.docs[i]['DateTime'],
                          id: data.docs[i]['id'],
                          lastMessage: data.docs[i]['lastMessage'],
                          ownerid: data.docs[i]['ownerID'],
                          receivername: data.docs[i]['receiverName'],
                          senduserid: data.docs[i]['sendUserId'],
                          sendername: data.docs[i]['senderName'],
                          unreadcount: data.docs[i]['unreadCount']);

                      array_conversationlist.add(modal);
                    }
                    print(array_conversationlist.length);
                  }
                  return Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white,
                      child: ListView.builder(
                          itemCount: array_conversationlist.toList().length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(
                                  top: 10),
                              child: ListTile(
                                onTap: () => {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => TestScreenPage(
                                              title: array_conversationlist[
                                                              index]
                                                          .sendername ==
                                                      LogineUsername
                                                  ? array_conversationlist[
                                                          index]
                                                      .receivername
                                                  : array_conversationlist[
                                                          index]
                                                      .sendername,
                                              id:
                                                  array_conversationlist[index]
                                                              .ownerid ==
                                                          LogineUserId
                                                      ? array_conversationlist[
                                                              index]
                                                          .senduserid
                                                      : array_conversationlist[
                                                              index]
                                                          .ownerid)))
                                },
                                title: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.blue[300],
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Container(
                                              child: Text(
                                                  array_conversationlist[
                                                                  index]
                                                              .sendername ==
                                                          LogineUsername
                                                      ? array_conversationlist[
                                                              index]
                                                          .receivername
                                                      : array_conversationlist[
                                                              index]
                                                          .sendername,
                                                  style:
                                                      TextStyle(fontSize: 18)),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        10, 0, 10, 10),
                                                child: Container(
                                                  child: Text(
                                                    array_conversationlist[
                                                            index]
                                                        .lastMessage,
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 50,
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        10, 0, 10, 10),
                                                child: Container(
                                                  child: Text(
                                                    array_conversationlist[
                                                            index]
                                                        .datetime,
                                                    style:
                                                        TextStyle(fontSize: 12),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }));
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
