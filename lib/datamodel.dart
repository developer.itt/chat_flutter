import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';

class DataModel {
  String name;
  String phone;
  String email;
  String age;
  String city;
  String id;

  DataModel(
      {required this.name,
      required this.phone,
      required this.email,
      required this.age,
      required this.city,
      required this.id});

  Map<String, dynamic> toJson() => {
        'name': name,
        'phone': phone,
        'email': email,
        'age': age,
        'city': city,
        'id': id
      };
  static DataModel fromJson(Map<String, dynamic> json) => DataModel(
      name: json['name'],
      phone: json['phone'],
      email: json['email'],
      age: json['age'],
      city: json['city'],
      id: json['id']);
}

class ChatModal {
  String datetime;
  String id;
  String message;
  String ownerid;
  String userids;
  String contenttype;
  bool isseen;

  ChatModal(
      {required this.datetime,
      required this.id,
      required this.message,
      required this.ownerid,
      required this.userids,
      required this.contenttype, 
      required this.isseen});

  Map<String, dynamic> toJson() => {
        'datetime': datetime,
        'id': id,
        'message': message,
        'ownerid': ownerid,
        'userids': userids,
        'contenttype': contenttype,
        'isseen': isseen
      };
  static ChatModal fromJson(Map<String, dynamic> json) => ChatModal(
        datetime: json['datetime'],
        id: json['id'],
        message: json['message'],
        ownerid: json['ownerid'],
        userids: json['userids'],
        contenttype: json['contenttype'],
        isseen: json['isseen']
      );
}

class ConversationModal {
  String datetime;
  String id;
  String lastMessage;
  String ownerid;
  String receivername;
  String senduserid;
  String sendername;
  String unreadcount;

  ConversationModal(
      {required this.datetime,
      required this.id,
      required this.lastMessage,
      required this.ownerid,
      required this.receivername,
      required this.senduserid,
      required this.sendername,
      required this.unreadcount,});

  Map<String, dynamic> toJson() => {
        'datetime': datetime,
        'id': id,
        'message': lastMessage,
        'ownerid': ownerid,
        'receivername': receivername,
        'senduserid': senduserid,
        'sendername': sendername,
        'unreadcount': unreadcount
      };
  static ConversationModal fromJson(Map<String, dynamic> json) =>
      ConversationModal(
        datetime: json['datetime'],
        id: json['id'],
        lastMessage: json['lastMessage'],
        ownerid: json['ownerid'],
        receivername: json['receivername'],
        senduserid: json['senduserid'],
        sendername: json['sendername'],
        unreadcount: json['unreadcount']);
}
