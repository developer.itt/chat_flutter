// ignore_for_file: import_of_legacy_library_into_null_safe

import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/button.dart';
import 'package:firebase_database/conversation.dart';
import 'package:firebase_database/login.dart';
import 'package:firebase_database/textfield.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/const.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_database/constants/app_constants.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: const MyHomePage(title: 'Firebase Saving Data'),
      home: LogineUserId == '' ? const MyHomePage(title: 'Firebase Saving Data') : ConversationScreenPage(title: 'Login with $LogineUsername'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController age = TextEditingController();
  TextEditingController city = TextEditingController();
  final String _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  final Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  Future<void> _submitclick() async {
    String userId = getRandomString(16);

    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('Name', 'Saurav');
    pref.setString('Phone', '812825456');
    pref.setString('Email', 'saurav@gmail.com');
    pref.setString('Age', '24');
    pref.setString('City', 'Palanpur');
    pref.setString('Id', userId);

    print(pref.getString('Name'));

    if (Nullcheck(name.text) == false) {
      Toast("Please enter name.", context);
      return;
    }
    if (Nullcheck(phone.text) == false) {
      Toast("Please enter phone.", context);
      return;
    }
    if (Nullcheck(email.text) == false) {
      Toast("Please enter email.", context);
      return;
    }
    if (Nullcheck(age.text) == false) {
      Toast("Please enter age.", context);
      return;
    }
    if (Nullcheck(city.text) == false) {
      Toast("Please enter city.", context);
      return;
    }

    DocumentReference users =
        FirebaseFirestore.instance.collection('users').doc(userId);

    users.set({
      'Name': name.text,
      'Phone': phone.text,
      'Email': email.text,
      'Age': age.text,
      'City': city.text,
      'Id': userId
    });

    name.clear;
    phone.clear;
    email.clear;
    age.clear;
    city.clear;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('Name', name.text);
    prefs.setString('Phone', phone.text);
    prefs.setString('Email', email.text);
    prefs.setString('Age', age.text);
    prefs.setString('City', city.text);
    prefs.setString('Id', userId);
    Toast("Data successfully added.", context);
  }

  void _navigatetonextscreen() {
    print(LogineUsername);

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ConversationScreenPage(
                title: 'Login with $LogineUsername',
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Textedit('Enter name', false, name),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Textedit('Enter phone', false, phone),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Textedit('Enter email', false, email),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Textedit('Enter age', false, age),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Textedit('Enter city', false, city),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
              child: Button('Submit', () => _submitclick),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
              child: Button('Show All Users', () => _navigatetonextscreen),
            ),
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
