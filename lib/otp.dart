// ignore_for_file: import_of_legacy_library_into_null_safe

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/button.dart';
import 'package:firebase_database/conversation.dart';
import 'package:firebase_database/textfield.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/const.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_database/constants/app_constants.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const OtpPage());
}

class OtpPage extends StatelessWidget {
  const OtpPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyOtpPage(title: 'Login'),
    );
  }
}

class MyOtpPage extends StatefulWidget {
  
  MyOtpPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyOtpPage> createState() => _MyOtpPageState();
}

class _MyOtpPageState extends State<MyOtpPage> {
  String _verificationID = '';

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  TextEditingController otp = TextEditingController();

  Future<void> verifyclick() async
  {
    if (Nullcheck(otp.text) == false) {
      Toast("Please enter otp.", context);
      return;
    }
    otp.clear;

    // Toast("Verify successfully.", context);

    try {
      await FirebaseAuth.instance
      .signInWithCredential(PhoneAuthProvider.credential(verificationId: _verificationID, smsCode: otp.text))
      .then((value) async {
        if (value.user != null){
          print('home');
        }
      });
    } catch (e) {
      print('invalid otp');
    }

    // await FirebaseAuth.instance.verifyPhoneNumber(phoneNumber: '+91${widget.title}', verificationCompleted: (PhoneAuthCredential credential) async {
    //   await FirebaseAuth.instance.signInWithCredential(credential)
    //   .then((value)  async {
    //     if (value.user != null) {
    //       print('user logged in');
    //     }
    //   });
    // }, verificationFailed: (FirebaseException e){
    //   print(e.message);
    // }, codeSent: (String verificationID, int? resendToken) {
    //   setState(() {
    //     _verificationID = verificationID;
    //   });
    // }, codeAutoRetrievalTimeout: (String verificationID) {
    //   setState(() {
    //       _verificationID = verificationID;
    //   });
    // }, 
    // timeout: Duration(seconds: 60));


    //   Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => ListingScreenPage(
    //             title: 'Login with $LogineUsername',
    //           )),
    // );
  }

  void _navigatetonextscreen() {
    print(LogineUsername);

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ConversationScreenPage(
                title: 'Login with $LogineUsername',
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Textedit('Enter phone otp', false, otp),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
                child: Button('Verify', () => verifyclick),
              ),
            ],
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
