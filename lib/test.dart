// ignore_for_file: unused_local_variable

import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
// ignore: unused_import
import 'package:firebase_database/constants/app_constants.dart';
import 'package:firebase_database/datamodel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';
// ignore: unused_import
import 'button.dart';
import 'constants/app_constants.dart';
import 'dart:async';

class TestScreenPage extends StatefulWidget {
  final String title;
  final String id;
  const TestScreenPage({Key? key, required this.title, required this.id})
      : super(key: key);
  @override
  State<TestScreenPage> createState() =>
      // ignore: no_logic_in_create_state
      _TestScreenPageState(name: title, id: id);
}

class _TestScreenPageState extends State<TestScreenPage> {
  // ignore: unused_element
  _TestScreenPageState({Key? key, required this.name, required this.id});
  String name;
  String id;
  final String _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  final Random _rnd = Random();
  // ignore: non_constant_identifier_names
  List<ChatModal> array_chat = [];
  int index = 0;
  String id1 = '';
  String id2 = '';
  // ignore: non_constant_identifier_names
  String PushId = 'user3 _user1';
  double keyboardHeight = 0.0;

  TextEditingController msg = TextEditingController();
  final Stream<QuerySnapshot> conversation =
      FirebaseFirestore.instance.collection('conversation').snapshots();
  final prefs = SharedPreferences.getInstance();

  Stream<QuerySnapshot<Map<String, dynamic>>> chat = FirebaseFirestore.instance
      .collection('conversation')
      .doc(pushid)
      .collection('Messages')
      .orderBy('timestamp')
      .snapshots();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  // ignore: unnecessary_new
  final ScrollController _scrollController = new ScrollController();

  Future<void> sendmessageClick() async {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('d MMM, yyyy h:mm a');
    final String datetime = formatter.format(now);
    String strId = getRandomString(16);

    if ((msg.text.isNotEmpty) && (msg.text != '')) {
      DocumentReference conversation = FirebaseFirestore.instance
          .collection('conversation')
          .doc(PushId)
          .collection('Messages')
          .doc(strId);
      conversation.set({
        'contentType': '0',
        'id': strId,
        'message': msg.text,
        'ownerID': 'loginuser_$LogineUserId',
        'timestamp': FieldValue.serverTimestamp(),
        'userIDs': ['user_$LogineUserId', 'user_$id'],
        'DateTime': datetime,
        'isseen': false
      });
      _scrollDown();
      FirebaseEvent();
      msg.clear();
    }
  }

  Future<void> _scrollDown() async {
    // _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    Timer(const Duration(seconds: 1), () {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 100), curve: Curves.easeInOut);
    });
  }

  // ignore: non_constant_identifier_names
  Future<void> FirebaseEvent() async {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('d MMM, yyyy h:mm a');
    final String datetime = formatter.format(now);

    DocumentReference conversation =
        FirebaseFirestore.instance.collection('conversation').doc(PushId);
    conversation.set({
      'DateTime': datetime,
      'id': PushId,
      'lastMessage': msg.text,
      'ownerID': LogineUserId,
      'receiverName': widget.title,
      'sendUserId': id,
      'senderName': LogineUsername,
      'timestamp': FieldValue.serverTimestamp(),
      'unreadCount': '0',
    });
  }

  // _listviewmessage(String id) async {
  //   if (id.isEmpty) {
  //     return;
  //   }
  //   StreamBuilder<QuerySnapshot>(
  //     stream: FirebaseFirestore.instance
  //         .collection('conversation')
  //         .doc(id)
  //         .collection('Messages')
  //         .orderBy('timestamp')
  //         .snapshots(),
  //     builder: (
  //       BuildContext context,
  //       AsyncSnapshot<QuerySnapshot> snapshot,
  //     ) {
  //       if (snapshot.hasError) {
  //         return const Text('Erro');
  //       }
  //       if (snapshot.connectionState == ConnectionState.waiting) {
  //         return const Text('Weiting');
  //       }
  //       final data = snapshot.requireData;
  //       array_chat = [];

  //       for (var i = 0; i < data.size; i++) {
  //         ChatModal modal = ChatModal(
  //             datetime: data.docs[i]['DateTime'],
  //             id: data.docs[i]['id'],
  //             message: data.docs[i]['message'],
  //             ownerid: data.docs[i]['ownerID'],
  //             userids: '',
  //             contenttype: data.docs[i]['contentType']);
  //         array_chat.add(modal);
  //       }

  //       return Center(
  //         child: Container(
  //             width: MediaQuery.of(context).size.width - 40,
  //             height: _keyboardIsVisible() == true
  //                 ? MediaQuery.of(context).size.height - (200 + keyboardHeight)
  //                 : MediaQuery.of(context).size.height - 200,
  //             // color: Colors.yellow,
  //             child: Padding(
  //               padding: const EdgeInsets.all(0.0),
  //               child: SizedBox(
  //                   height: MediaQuery.of(context).size.height,
  //                   width: MediaQuery.of(context).size.width,
  //                   child: ListView.builder(
  //                       controller: _scrollController,
  //                       shrinkWrap: true,
  //                       itemCount: array_chat.toList().length,
  //                       itemBuilder: (context, index) {
  //                         if (array_chat[index].ownerid == 'loginuser_$id') {
  //                           return Padding(
  //                             padding:
  //                                 const EdgeInsets.only(top: 10, bottom: 10),
  //                             child: Column(
  //                               crossAxisAlignment: CrossAxisAlignment.start,
  //                               children: [
  //                                 Container(
  //                                   decoration: BoxDecoration(
  //                                       color: Colors.blue[200],
  //                                       borderRadius: BorderRadius.circular(8)),
  //                                   width:
  //                                       MediaQuery.of(context).size.width - 150,
  //                                   child: Column(
  //                                     mainAxisAlignment:
  //                                         MainAxisAlignment.start,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.start,
  //                                     children: [
  //                                       Padding(
  //                                         padding: const EdgeInsets.all(10.0),
  //                                         child: Text(
  //                                           array_chat[index].message,
  //                                           style: const TextStyle(
  //                                               fontSize: 18,
  //                                               fontWeight: FontWeight.bold),
  //                                         ),
  //                                       ),
  //                                       Padding(
  //                                         padding: const EdgeInsets.only(
  //                                             left: 10, right: 10, bottom: 10),
  //                                         child: Text(
  //                                             array_chat[index].datetime,
  //                                             style: const TextStyle(
  //                                                 fontWeight: FontWeight.bold,
  //                                                 fontSize: 12,
  //                                                 color: Colors.grey)),
  //                                       )
  //                                     ],
  //                                   ),
  //                                 )
  //                               ],
  //                             ),
  //                           );
  //                         } else {
  //                           return Padding(
  //                             padding:
  //                                 const EdgeInsets.only(top: 10, bottom: 10),
  //                             child: Column(
  //                               crossAxisAlignment: CrossAxisAlignment.end,
  //                               children: [
  //                                 Container(
  //                                   decoration: BoxDecoration(
  //                                       color: Colors.orange[200],
  //                                       borderRadius: BorderRadius.circular(8)),
  //                                   width:
  //                                       MediaQuery.of(context).size.width - 150,
  //                                   child: Column(
  //                                     mainAxisAlignment: MainAxisAlignment.end,
  //                                     crossAxisAlignment:
  //                                         CrossAxisAlignment.end,
  //                                     children: [
  //                                       Padding(
  //                                         padding: const EdgeInsets.all(10.0),
  //                                         child: Text(
  //                                           array_chat[index].message,
  //                                           style: const TextStyle(
  //                                               fontSize: 18,
  //                                               fontWeight: FontWeight.bold),
  //                                         ),
  //                                       ),
  //                                       Padding(
  //                                         padding: const EdgeInsets.only(
  //                                             left: 10, right: 10, bottom: 10),
  //                                         child: Text(
  //                                             array_chat[index].datetime,
  //                                             style: const TextStyle(
  //                                                 fontSize: 12,
  //                                                 color: Colors.grey)),
  //                                       )
  //                                     ],
  //                                   ),
  //                                 )
  //                               ],
  //                             ),
  //                           );
  //                         }
  //                       })),
  //             )),
  //       );
  //     },
  //   );
  // }

  bool _keyboardIsVisible() {
    _scrollDown();
    keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
    return !(MediaQuery.of(context).viewInsets.bottom == 0.0);
  }

  @override
  Widget build(BuildContext context) {
    var test;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              StreamBuilder<QuerySnapshot>(
                stream: conversation,
                builder: (
                  BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot,
                ) {
                  if (snapshot.hasError) {
                    return const Text('');
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Text('');
                  }
                  final data = snapshot.requireData;
                  id1 = 'user$LogineUserId _user$id';
                  id2 = 'user$id _user$LogineUserId';
                  print(id1);
                  print(id2);

                  int created = 50;
                  for (var i = 0; i < data.size; i++) {
                    if (data.docs[i].id.toString() == id1) {
                      created = 1;
                      PushId = id1;
                      pushid = id1;
                      return StreamBuilder<QuerySnapshot>(
                        stream: FirebaseFirestore.instance
                            .collection('conversation')
                            .doc(id1)
                            .collection('Messages')
                            .orderBy('timestamp')
                            .snapshots(),
                        builder: (
                          BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot,
                        ) {
                          if (snapshot.hasError) {
                            return const Text('Erro');
                          }
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const Text('Weiting');
                          }
                          final data = snapshot.requireData;
                          array_chat = [];

                          for (var i = 0; i < data.size; i++) {
                            print((data.docs[i]['ownerID']));
                            print('DONE loginuser_$LogineUserId');

                            if (data.docs[i]['ownerID'] !=
                                'loginuser_$LogineUserId') {
                              print("ISSEEN: TRUE");
                              DocumentReference ref = FirebaseFirestore.instance
                                  .collection('conversation')
                                  .doc(PushId)
                                  .collection('Messages')
                                  .doc(data.docs[i]['id']);
                              ref.update({'isseen': true});
                            }

                            ChatModal modal = ChatModal(
                                datetime: data.docs[i]['DateTime'],
                                id: data.docs[i]['id'],
                                message: data.docs[i]['message'],
                                ownerid: data.docs[i]['ownerID'],
                                userids: '',
                                contenttype: data.docs[i]['contentType'],
                                isseen: data.docs[i]['isseen']);
                            array_chat.add(modal);
                          }

                          print(array_chat.length);

                          return Center(
                            child: Container(
                                width: MediaQuery.of(context).size.width - 40,
                                height: _keyboardIsVisible() == true
                                    ? MediaQuery.of(context).size.height -
                                        (200 + keyboardHeight)
                                    : MediaQuery.of(context).size.height - 200,
                                // color: Colors.yellow,
                                child: Padding(
                                  padding: const EdgeInsets.all(0.0),
                                  child: SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height,
                                      width: MediaQuery.of(context).size.width,
                                      child: ListView.builder(
                                          controller: _scrollController,
                                          shrinkWrap: true,
                                          itemCount: array_chat.toList().length,
                                          itemBuilder: (context, index) {
                                            if (array_chat[index].ownerid ==
                                                'loginuser_$id') {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 10, bottom: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color:
                                                              Colors.blue[200],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              150,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(10.0),
                                                            child: Text(
                                                              array_chat[index]
                                                                  .message,
                                                              style: const TextStyle(
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10,
                                                                    right: 10,
                                                                    bottom: 10),
                                                            child: Text(
                                                                array_chat[
                                                                        index]
                                                                    .datetime,
                                                                style: const TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        12,
                                                                    color: Colors
                                                                        .grey)),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            } else {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 10, bottom: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color: Colors
                                                              .orange[200],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              150,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .end,
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(10.0),
                                                            child: Text(
                                                              array_chat[index]
                                                                  .message,
                                                              style: const TextStyle(
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10,
                                                                    right: 10,
                                                                    bottom: 10),
                                                            child: Text(
                                                                array_chat[
                                                                        index]
                                                                    .datetime,
                                                                style: const TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    color: Colors
                                                                        .grey)),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10,
                                                                    right: 10,
                                                                    bottom: 10),
                                                            child: Image(
                                                                image: array_chat[index]
                                                                            .isseen ==
                                                                        false
                                                                    ? const AssetImage(
                                                                        'assets/images/check.png')
                                                                    : const AssetImage(
                                                                        'assets/images/read.png')),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            }
                                          })),
                                )),
                          );
                        },
                      );
                    } else if (data.docs[i].id.toString() == id2) {
                      created = 1;
                      PushId = id2;
                      pushid = id2;
                      print('MATCHED: $PushId');
                      return StreamBuilder<QuerySnapshot>(
                        stream: FirebaseFirestore.instance
                            .collection('conversation')
                            .doc(id2)
                            .collection('Messages')
                            .orderBy('timestamp')
                            .snapshots(),
                        builder: (
                          BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot,
                        ) {
                          if (snapshot.hasError) {
                            return const Text('Erro');
                          }
                          if (snapshot.connectionState ==
                              ConnectionState.waiting) {
                            return const Text('Weiting');
                          }
                          final data = snapshot.requireData;
                          array_chat = [];

                          for (var i = 0; i < data.size; i++) {
                            print((data.docs[i]['ownerID']));
                            print('DONE loginuser_$LogineUserId');

                            if (data.docs[i]['ownerID'] !=
                                'loginuser_$LogineUserId') {
                              print("ISSEEN: TRUE");
                              DocumentReference ref = FirebaseFirestore.instance
                                  .collection('conversation')
                                  .doc(PushId)
                                  .collection('Messages')
                                  .doc(data.docs[i]['id']);
                              ref.update({'isseen': true});
                            }

                            ChatModal modal = ChatModal(
                                datetime: data.docs[i]['DateTime'],
                                id: data.docs[i]['id'],
                                message: data.docs[i]['message'],
                                ownerid: data.docs[i]['ownerID'],
                                userids: '',
                                contenttype: data.docs[i]['contentType'],
                                isseen: data.docs[i]['isseen']);
                            array_chat.add(modal);
                          }

                          return Center(
                            child: Container(
                                width: MediaQuery.of(context).size.width - 40,
                                height: _keyboardIsVisible() == true
                                    ? MediaQuery.of(context).size.height -
                                        (200 + keyboardHeight)
                                    : MediaQuery.of(context).size.height - 200,
                                // color: Colors.yellow,
                                child: Padding(
                                  padding: const EdgeInsets.all(0.0),
                                  child: SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height,
                                      width: MediaQuery.of(context).size.width,
                                      child: ListView.builder(
                                          controller: _scrollController,
                                          shrinkWrap: true,
                                          itemCount: array_chat.toList().length,
                                          itemBuilder: (context, index) {
                                            if (array_chat[index].ownerid ==
                                                'loginuser_$id') {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 10, bottom: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color:
                                                              Colors.blue[200],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              150,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(10.0),
                                                            child: Text(
                                                              array_chat[index]
                                                                  .message,
                                                              style: const TextStyle(
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10,
                                                                    right: 10,
                                                                    bottom: 10),
                                                            child: Text(
                                                                array_chat[
                                                                        index]
                                                                    .datetime,
                                                                style: const TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        12,
                                                                    color: Colors
                                                                        .grey)),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            } else {
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 10, bottom: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color: Colors
                                                              .orange[200],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(8)),
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              150,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .end,
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(10.0),
                                                            child: Text(
                                                              array_chat[index]
                                                                  .message,
                                                              style: const TextStyle(
                                                                  fontSize: 18,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10,
                                                                    right: 10,
                                                                    bottom: 10),
                                                            child: Text(
                                                                array_chat[
                                                                        index]
                                                                    .datetime,
                                                                style: const TextStyle(
                                                                    fontSize:
                                                                        12,
                                                                    color: Colors
                                                                        .grey)),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10,
                                                                    right: 10,
                                                                    bottom: 10),
                                                            child: Image(
                                                                image: array_chat[index]
                                                                            .isseen ==
                                                                        false
                                                                    ? const AssetImage(
                                                                        'assets/images/check.png')
                                                                    : const AssetImage(
                                                                        'assets/images/read.png')),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              );
                                            }
                                          })),
                                )),
                          );
                        },
                      );
                    }
                  }
                  if (created != 1) {
                    PushId = id1;
                    pushid = id1;
                    return StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('conversation')
                          .doc(id1)
                          .collection('Messages')
                          .orderBy('timestamp')
                          .snapshots(),
                      builder: (
                        BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot,
                      ) {
                        if (snapshot.hasError) {
                          return const Text('Erro');
                        }
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Text('Weiting');
                        }
                        final data = snapshot.requireData;
                        array_chat = [];

                        for (var i = 0; i < data.size; i++) {
                          print((data.docs[i]['ownerID']));
                          print('DONE loginuser_$LogineUserId');

                          if (data.docs[i]['ownerID'] !=
                              'loginuser_$LogineUserId') {
                            print("ISSEEN: TRUE");
                            DocumentReference ref = FirebaseFirestore.instance
                                .collection('conversation')
                                .doc(PushId)
                                .collection('Messages')
                                .doc(data.docs[i]['id']);
                            ref.update({'isseen': true});
                          }
                          ChatModal modal = ChatModal(
                              datetime: data.docs[i]['DateTime'],
                              id: data.docs[i]['id'],
                              message: data.docs[i]['message'],
                              ownerid: data.docs[i]['ownerID'],
                              userids: '',
                              contenttype: data.docs[i]['contentType'],
                              isseen: data.docs[i]['isseen']);
                          array_chat.add(modal);
                        }

                        return Center(
                          child: Container(
                              width: MediaQuery.of(context).size.width - 40,
                              height: _keyboardIsVisible() == true
                                  ? MediaQuery.of(context).size.height -
                                      (200 + keyboardHeight)
                                  : MediaQuery.of(context).size.height - 200,
                              // color: Colors.yellow,
                              child: Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: SizedBox(
                                    height: MediaQuery.of(context).size.height,
                                    width: MediaQuery.of(context).size.width,
                                    child: ListView.builder(
                                        controller: _scrollController,
                                        shrinkWrap: true,
                                        itemCount: array_chat.toList().length,
                                        itemBuilder: (context, index) {
                                          if (array_chat[index].ownerid ==
                                              'loginuser_$id') {
                                            return Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10, bottom: 10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    decoration: BoxDecoration(
                                                        color: Colors.blue[200],
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8)),
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width -
                                                            150,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          child: Text(
                                                            array_chat[index]
                                                                .message,
                                                            style: const TextStyle(
                                                                fontSize: 18,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 10,
                                                                  right: 10,
                                                                  bottom: 10),
                                                          child: Text(
                                                              array_chat[index]
                                                                  .datetime,
                                                              style: const TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize: 12,
                                                                  color: Colors
                                                                      .grey)),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            );
                                          } else {
                                            return Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 10, bottom: 10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Container(
                                                    decoration: BoxDecoration(
                                                        color:
                                                            Colors.orange[200],
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(8)),
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width -
                                                            150,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.end,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .end,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          child: Text(
                                                            array_chat[index]
                                                                .message,
                                                            style: const TextStyle(
                                                                fontSize: 18,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 10,
                                                                  right: 10,
                                                                  bottom: 10),
                                                          child: Text(
                                                              array_chat[index]
                                                                  .datetime,
                                                              style: const TextStyle(
                                                                  fontSize: 12,
                                                                  color: Colors
                                                                      .grey)),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 10,
                                                                  right: 10,
                                                                  bottom: 10),
                                                          child: Image(
                                                            image: array_chat[
                                                                            index]
                                                                        .isseen ==
                                                                    false
                                                                ? const AssetImage(
                                                                    'assets/images/check.png')
                                                                : const AssetImage(
                                                                    'assets/images/read.png'),
                                                            fit: BoxFit.cover,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            );
                                          }
                                        })),
                              )),
                        );
                      },
                    );
                  }
                  print('PUSHID: $PushId');
                  return const Text('');
                },
              ),
              // StreamBuilder<QuerySnapshot>(
              //   stream: FirebaseFirestore.instance
              //       .collection('conversation')
              //       .doc(PushId)
              //       .collection('Messages')
              //       .orderBy('timestamp')
              //       .snapshots(),
              //   builder: (
              //     BuildContext context,
              //     AsyncSnapshot<QuerySnapshot> snapshot,
              //   ) {
              //     if (snapshot.hasError) {
              //       return const Text('Erro');
              //     }
              //     if (snapshot.connectionState == ConnectionState.waiting) {
              //       return const Text('Weiting');
              //     }
              //     final data = snapshot.requireData;
              //     array_chat = [];

              //     for (var i = 0; i < data.size; i++) {
              //       ChatModal modal = ChatModal(
              //           datetime: data.docs[i]['DateTime'],
              //           id: data.docs[i]['id'],
              //           message: data.docs[i]['message'],
              //           ownerid: data.docs[i]['ownerID'],
              //           userids: '',
              //           contenttype: data.docs[i]['contentType']);
              //       array_chat.add(modal);
              //     }

              //     return Center(
              //       child: Container(
              //           width: MediaQuery.of(context).size.width - 40,
              //           height: _keyboardIsVisible() == true
              //               ? MediaQuery.of(context).size.height -
              //                   (200 + keyboardHeight)
              //               : MediaQuery.of(context).size.height - 200,
              //           // color: Colors.yellow,
              //           child: Padding(
              //             padding: const EdgeInsets.all(0.0),
              //             child: SizedBox(
              //                 height: MediaQuery.of(context).size.height,
              //                 width: MediaQuery.of(context).size.width,
              //                 child: ListView.builder(
              //                     controller: _scrollController,
              //                     shrinkWrap: true,
              //                     itemCount: array_chat.toList().length,
              //                     itemBuilder: (context, index) {
              //                       if (array_chat[index].ownerid ==
              //                           'loginuser_$id') {
              //                         return Padding(
              //                           padding: const EdgeInsets.only(
              //                               top: 10, bottom: 10),
              //                           child: Column(
              //                             crossAxisAlignment:
              //                                 CrossAxisAlignment.start,
              //                             children: [
              //                               Container(
              //                                 decoration: BoxDecoration(
              //                                     color: Colors.blue[200],
              //                                     borderRadius:
              //                                         BorderRadius.circular(8)),
              //                                 width: MediaQuery.of(context)
              //                                         .size
              //                                         .width -
              //                                     150,
              //                                 child: Column(
              //                                   mainAxisAlignment:
              //                                       MainAxisAlignment.start,
              //                                   crossAxisAlignment:
              //                                       CrossAxisAlignment.start,
              //                                   children: [
              //                                     Padding(
              //                                       padding:
              //                                           const EdgeInsets.all(
              //                                               10.0),
              //                                       child: Text(
              //                                         array_chat[index].message,
              //                                         style: const TextStyle(
              //                                             fontSize: 18,
              //                                             fontWeight:
              //                                                 FontWeight.bold),
              //                                       ),
              //                                     ),
              //                                     Padding(
              //                                       padding:
              //                                           const EdgeInsets.only(
              //                                               left: 10,
              //                                               right: 10,
              //                                               bottom: 10),
              //                                       child: Text(
              //                                           array_chat[index]
              //                                               .datetime,
              //                                           style: const TextStyle(
              //                                               fontWeight:
              //                                                   FontWeight.bold,
              //                                               fontSize: 12,
              //                                               color:
              //                                                   Colors.grey)),
              //                                     )
              //                                   ],
              //                                 ),
              //                               )
              //                             ],
              //                           ),
              //                         );
              //                       } else {
              //                         return Padding(
              //                           padding: const EdgeInsets.only(
              //                               top: 10, bottom: 10),
              //                           child: Column(
              //                             crossAxisAlignment:
              //                                 CrossAxisAlignment.end,
              //                             children: [
              //                               Container(
              //                                 decoration: BoxDecoration(
              //                                     color: Colors.orange[200],
              //                                     borderRadius:
              //                                         BorderRadius.circular(8)),
              //                                 width: MediaQuery.of(context)
              //                                         .size
              //                                         .width -
              //                                     150,
              //                                 child: Column(
              //                                   mainAxisAlignment:
              //                                       MainAxisAlignment.end,
              //                                   crossAxisAlignment:
              //                                       CrossAxisAlignment.end,
              //                                   children: [
              //                                     Padding(
              //                                       padding:
              //                                           const EdgeInsets.all(
              //                                               10.0),
              //                                       child: Text(
              //                                         array_chat[index].message,
              //                                         style: const TextStyle(
              //                                             fontSize: 18,
              //                                             fontWeight:
              //                                                 FontWeight.bold),
              //                                       ),
              //                                     ),
              //                                     Padding(
              //                                       padding:
              //                                           const EdgeInsets.only(
              //                                               left: 10,
              //                                               right: 10,
              //                                               bottom: 10),
              //                                       child: Text(
              //                                           array_chat[index]
              //                                               .datetime,
              //                                           style: const TextStyle(
              //                                               fontSize: 12,
              //                                               color:
              //                                                   Colors.grey)),
              //                                     )
              //                                   ],
              //                                 ),
              //                               )
              //                             ],
              //                           ),
              //                         );
              //                       }
              //                     })),
              //           )),
              //     );
              //   },
              // ),
            ],
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      height: 50,
                      width: MediaQuery.of(context).size.width - 100,
                      // color: Colors.yellowAccent,
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(8)),
                      child: TextFormField(
                        controller: msg,
                        style: const TextStyle(color: Colors.black),
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Type message to send',
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                        keyboardType: TextInputType.name,
                        textInputAction: TextInputAction.next,
                      ),
                    ),
                    const SizedBox(width: 10),
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(8)),
                      child: IconButton(
                        icon: const Icon(Icons.send),
                        color: Colors.black,
                        onPressed: () => sendmessageClick(),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
