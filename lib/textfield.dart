import 'package:flutter/material.dart';

class Textedit extends StatelessWidget {
  String placeholder;
  bool hidetext;
  TextEditingController controller = new TextEditingController();

  Textedit(this.placeholder, this.hidetext, this.controller);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.visiblePassword,
        decoration: InputDecoration(
          hintText: placeholder,
          labelStyle: TextStyle(color: Colors.grey, fontSize: 18),
          labelText: placeholder,
        ),
        obscureText: hidetext,
        onChanged: (text) {
          print('Text field: $text');
        },
      ),
    );
  }
}
